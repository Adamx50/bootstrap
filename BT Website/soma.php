<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
     integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
     <link rel="stylesheet" href="style.css">

</head>
<body>
    <!--navbar-->
 <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="index.html"><img src="images/logo.jpg"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-200px ">
          <li class="nav-item">
            <a class="nav-link " href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="about.php">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Products
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="#">Crime City-Mobile</a></li>
              <li><a class="dropdown-item" href="#">Fighters War</a></li>
              <li><a class="dropdown-item" href="soma.php">Soma</a></li>
            </ul>
            
          </li>
          <li class="nav-item">
          <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='message.php'>Contact</a>";
           }
        }else{
             
            echo "<a class='nav-link active' href='contact.php'>Contact</a>";
           }
        
            ?>
          </li>

          <li > <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='user.php'>Profile</a>";
           }
        }else{
             
            echo " ";
           }
        
            ?> </li>
        </ul>  
    
      
      
      </div>
      <?php
     if(isset($_SESSION['id'])){
     if(session_status() == PHP_SESSION_ACTIVE){
    echo "<a href='logout.php' class='logout'> Kijelentkezés </a>";
    echo  '&nbsp&nbsp  Üdv ' ;
    echo $_SESSION['name'];
     }
    }else{
      echo "<a href='login.php' class='logout'> Bejelentkezés</a>" ;  
    }
  
       
      
      ?>
    </div>
  </nav>


    
  <div class="gamecontent">  
        
        <table  >
            <tr >

<td  style=width:70%  >


            <div id="carouselExampleControls" class="carousel slide col-lg-8 col-md-4 pr-50" data-bs-ride="carousel" >
    <div class="carousel-inner" >
      <div class="carousel-item active">
        <img src="images/soma1.jpeg"  class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/soma2.jpg" class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/soma3.jpg "  class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/soma4.jpg "  class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/soma5.jpg "  class="d-block w-100" alt="...">
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>

</td>

<td style=width:30% >
    <div class="about p-10">
       <img src="images/soma.jpg" style="height: 200px;"  >
       <p>
          Gerne : horror
          <br>
          Publisher : Frictional Games
          <br>
          Release date: 2015 Sep 7
          <br>
          Platform
          <br>
       <img>
       </p>
<button class="btn btn-danger">Buy on Steam</button>
    </div>


</td>
</tr>
<tr>

    <td style="width: 30%;" class="desc">
        <h1 class="title">Soma</h1>
        <p>
          <strong>About -</strong> SOMA is a sci-fi horror game from Frictional Games, the creators of Amnesia: The Dark Descent.
             It is an unsettling story about identity, consciousness, and what it means to be human.   
        </p>
        <p>
            <strong>Plot -</strong>  The radio is dead, food is running out, and the machines have started to think they are people. Underwater facility PATHOS-II has suffered an intolerable isolation and we’re going to have to make some tough decisions. What can be done? What makes sense? What is left to fight for?
        </p>
       
        <p>
            <strong>Features -</strong>  Enter the world of SOMA and face horrors buried deep beneath the ocean waves. Delve through locked terminals and secret documents to uncover the truth behind the chaos. Seek out the last remaining inhabitants and take part in the events that will ultimately shape the fate of the station. But be careful, danger lurks in every corner: corrupted humans, twisted creatures, insane robots, and even an inscrutable omnipresent A.I.
You will need to figure out how to deal with each one of them. Just remember there’s no fighting back, either you outsmart your enemies or you get 
ready to run.
        </p>

    </td>
</tr> 

</table>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<footer class="footer col-md-8 col-lg-4 ml-auto" style="padding-left: 50px;">
  <p> 2021 - Company &copy All rights reserved</p>
  
  </footer>
</body>
</html>