-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2021. Már 17. 11:30
-- Kiszolgáló verziója: 10.4.17-MariaDB
-- PHP verzió: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `user`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `profileimg`
--

CREATE TABLE `profileimg` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- A tábla adatainak kiíratása `profileimg`
--

INSERT INTO `profileimg` (`id`, `userid`, `status`) VALUES
(1, 13, 0),
(2, 14, 0),
(3, 15, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `user_name`, `password`, `name`) VALUES
(3, 'ela', '202cb962ac59075b964b07152d234b70', 'Ela'),
(4, 'elias', '202cb962ac59075b964b07152d234b70', 'elias');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `usertable`
--

CREATE TABLE `usertable` (
  `name` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `usertable`
--

INSERT INTO `usertable` (`name`, `pass`, `email`) VALUES
('aaa', '0cc175b9c0f1b6a831c399e269772661', 'a@email.hu'),
('Adam', '827ccb0eea8a706c4c34a16891f84e7b', 'Adamx5050@gmail.com');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `usertable1`
--

CREATE TABLE `usertable1` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- A tábla adatainak kiíratása `usertable1`
--

INSERT INTO `usertable1` (`id`, `name`, `email`, `pass`) VALUES
(1, 'Brad', 'b@email.hu', '65ba841e01d6db7733e90a5b7f9e6f80');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `usertable2`
--

CREATE TABLE `usertable2` (
  `name` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `usertable2`
--

INSERT INTO `usertable2` (`name`, `pass`, `email`) VALUES
('Adam', ' 12345', ' Adamx5050@gmail.com'),
('dan', ' ddd', ' d@email.hu');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `usertable3`
--

CREATE TABLE `usertable3` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `usertable3`
--

INSERT INTO `usertable3` (`id`, `name`, `email`, `pass`) VALUES
(13, 'Chris', 'c@email.com', 'c2e3287f4560195cc10102efe47768c1'),
(14, 'Adam', 'Adamx5050@gmail.com', '2cbe7f341eb6aca638a32b77ddedfd4c'),
(15, 'Brad', 'b@email.hu', 'dd1c83f31a99d0c8d26d9353279e438f');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `usertable4`
--

CREATE TABLE `usertable4` (
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `profileimg`
--
ALTER TABLE `profileimg`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `usertable`
--
ALTER TABLE `usertable`
  ADD PRIMARY KEY (`name`);

--
-- A tábla indexei `usertable1`
--
ALTER TABLE `usertable1`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `usertable2`
--
ALTER TABLE `usertable2`
  ADD PRIMARY KEY (`name`);

--
-- A tábla indexei `usertable3`
--
ALTER TABLE `usertable3`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `profileimg`
--
ALTER TABLE `profileimg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `usertable1`
--
ALTER TABLE `usertable1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `usertable3`
--
ALTER TABLE `usertable3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
