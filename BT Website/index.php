<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BT</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
 integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
 <link rel="stylesheet" href="style.css">
</head>
<body>
  <!--navbar-->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="images/logo.jpg"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-200px" >
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Products
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="">Crime City-Mobile</a></li>
              <li><a class="dropdown-item" href="#">Fighters War</a></li>
              <li><a class="dropdown-item" href="soma.php">Soma</a></li>
            </ul>
            
          </li>
          <li class="nav-item">
          <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='message.php'>Contact</a>";
           }
        }else{
             
            echo "<a class='nav-link active' href='contact.php'>Contact</a>";
           }
        
            ?>
          </li>

          <li > <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='user.php'>Profile</a>";
           }
        }else{
             
            echo " ";
           }
        
            ?> </li>
        </ul> 

          
      </div>
      
      <?php
     if(isset($_SESSION['id'])){
     if(session_status() == PHP_SESSION_ACTIVE){
    echo "<a href='logout.php' class='logout'> Kijelentkezés </a>";
    echo  '&nbsp&nbsp  Üdv ' ;
    echo $_SESSION['name'];
     }
    }else{
      echo "<a href='login.php' class='logout'> Bejelentkezés</a>" ;  
    }
  
       
      
      ?>
     
    
    
  
    
 

    </div>
    
  </nav>

   <!--carousel-->
   <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="images/game.jpg" height="700" class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/mobile.jpg" height="700" class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/web.jpg " height="700" class="d-block w-100" alt="...">
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>

<div class="container pt-4">
<!--About-->
<div class="row">
    <div class="col-lg">
<h3 class="mb-4">Welcome to our website!</h3>
<img src="images/office.jpg" class="img-fluid mb-4 rounded" alt="office">
    </div>
 <div class="col-lg">
        <p>Video Game Developer
            Based In La Rochelle, France.
            Part of the Adam Games family since 1999,
             Adam North is home to the Fighters War Series. Ambition and hard work have made us a preeminent games developer worldwide, and at the heart of it all is a team of motivated individuals who strive to produce amazing, industry-leading games.</p>
    </div>
    <div class="col-lg" >
        <strong> INDUSTRY</strong>	Interactive entertainment
        <br>
        <strong>FOUNDED</strong>	2001 as Adam North
        <br>
        <strong > PARENT</strong>   Adam Games
        <br>
        <strong>PRODUCTS</strong> Fighters War, Soma, Crime City-Mobile
    </div>
</div>
<!--videos-->
<h1 class="display-3 text-center text-muted my-4">Videos</h1>
<div class="row">
    <div class="col-md-6 col-lg-4">
        <div class="card mb-3">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/gwGOkKdb-SM" frameborder="0" allow="accelerometer; autoplay; 
            clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="card-body">
              <h5 class="card-title">How video games are made</h5>
              <p class="card-text">Want to know how video games are made? This article walks through the video game development process, and in particular, the 'pipeline' – an industry term that refers to the process of making a video game from scratch..</p>
              <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
          </div>
    </div>
    <div class="col-md-6 col-lg-4">
        <div class="card mb-3">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/uwxZVoqSwFQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
            allowfullscreen></iframe>
            <div class="card-body">
              <h5 class="card-title">Why to subscribe GameDev</h5>
              <p class="card-text">Game dev is a...</p>
              <p class="card-text"><small class="text-muted">Last updated 5 mins ago</small></p>
            </div>
          </div>
    </div>
    <div class="col-md-6 col-lg-4">
        <div class="card mb-3">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/fba2jz1XwqQ"
             frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; 
             gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="card-body">
              <h5 class="card-title">Best Languages for Game Development</h5>
              <p class="card-text">Find it out</p>
              <p class="card-text"><small class="text-muted">Last updated 8 mins ago</small></p>
            </div>
          </div>
    </div>
</div>

<!--newsletter-->
<div class="row py-4 text-muted">
    <div class="col-md">
      
    </div>
    <div class="col-md col-lg-5 ml-auto">
        <p> Weekly newsletter. Subscribe for update</p>
        <p><strong>Enter your email adress</strong> </p>
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="email adress" aria-label="email adress" aria-describedby="button-addon2">
            <button class="btn btn-outline-primary" type="button" id="button-addon2">Button</button>
          </div>
    </div>
    <div class="col-md-7"></div>
<ul class="nav">
    <li class="nav-item">
     <!-- Example single danger button -->
<div class="btn-group dropup pb-10">
    <button type="button"  class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
      Social Media
    </button>
    <ul class="dropdown-menu">
      <li><a class="dropdown-item" href="https://www.instagram.csom">Instagram <img src="images/ig.jpg"></a></li>
      <li><a class="dropdown-item" href="https://www.facebook.com">Facebook <img src="images/fb.png" style="padding-left:5px;"></a></li>
      <li><a class="dropdown-item" href="https://www.youtube.com">YouTube <img src="images/yt.png" style="padding-left:10px;"></a></li>
      
    </ul>
  </div>
    </li>
   
  </ul>
</div>


<!--footer-->



<footer class="footer col-md-8 col-lg-4 ml-auto" style="padding-left: 50px;">
  <p> 2021 - Company &copy All rights reserved</p>
  
  </footer>









 

    
    <!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>
</html>