<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
    rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  </head>
</head>
<body>

   <!--navbar-->
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="images/logo.jpg"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-200px" >
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Products
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="">Crime City-Mobile</a></li>
              <li><a class="dropdown-item" href="#">Fighters War</a></li>
              <li><a class="dropdown-item" href="soma.php">Soma</a></li>
            </ul>
          </li>
          <li class="nav-item">
          <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='message.php'>Contact</a>";
           }
        }else{
             
            echo "<a class='nav-link active' href='contact.php'>Contact</a>";
           }
        
            ?>
          </li>
          
          <li > <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='user.php'>Profile</a>";
           }
        }else{
             
            echo " ";
           }
        
            ?> </li>
          

        </ul>
      
       
      </div>
         
      <?php
     if(isset($_SESSION['id'])){
     if(session_status() == PHP_SESSION_ACTIVE){
    echo "<a href='logout.php' class='logout'> Kijelentkezés </a>";
    echo  '&nbsp&nbsp  Üdv ' ;
    echo $_SESSION['name'];
     }
    }else{
      echo "<a href='login.php' class='logout'> Bejelentkezés</a>" ;  
    }
  
       
      
      ?>
    </div>
    
  </nav>


<div class="contentForm" >

      <div class="col-1 mx-auto">
          <div class="jumbotron">
            <form class="registerform" id="form" action="messagecheck.php" method="POST">
            <h4 class="notification"></h4>
            <div class="form-group" style="padding-top: 80px;">
            <h2>Üzenet</h2>
            <?php if (isset($_GET['error'])) { ?>
     		<p class="error"><?php echo $_GET['error']; ?></p>
     	<?php } ?>
       <?php if (isset($_GET['success'])) { ?>
     		<p class="success"><?php echo $_GET['success']; ?></p>
     	<?php } ?>
      
              <label for="email">Email cím</label>
              <input type="text" class="form-control" id="email" name="email" placeholder="Email cím" required>
            </div>
            <div class="form-group">
              <label for="subject">Tárgy</label>
              <input type="text" class="form-control" id="subject" name="subject" placeholder="Az üzenet tárgya" required>
            </div>
            <div class="form-group">
              <label for="message">Üzenet</label>
             <textarea id="message" class="form-control" name="message" placeholder="Írja meg az üzenetét" rows="13" cols="40" ></textarea>

            </div>
            <div class="g-recaptcha" data-sitekey="6LevoXkaAAAAAH-qySx44CyjPsphBUEZ3am23FLf"></div>
            <button type="submit" name="submit" class="btn btn-primary">Küldés</button>
          </form>
          </div>

      </div>

</div>




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
 integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
 <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
 
 

<footer class="footer col-md-8 col-lg-4 ml-auto" style="padding-left: 50px;">
  <p> 2021 - Company &copy All rights reserved</p>

  </footer>
</body>
</html>