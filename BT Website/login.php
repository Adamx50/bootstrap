<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="valid.js">

    </script>
</head>
<body>

   <!--navbar-->
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="images/logo.jpg"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-200px" >
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="index.php" onclick="$loggedin=false">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Products
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="">Crime City-Mobile</a></li>
              <li><a class="dropdown-item" href="#">Fighters War</a></li>
              <li><a class="dropdown-item" href="soma.php">Soma</a></li>
            </ul>
            
          </li>
          <li class="nav-item">
          <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='message.php'>Contact</a>";
           }
        }else{
             
            echo "<a class='nav-link active' href='contact.php'>Contact</a>";
           }
        
            ?>
          </li>
        </ul>  
        
      </div>
      <?php
 
      session_start();
     
     if(isset($_SESSION['id'])){
     if(session_status() == PHP_SESSION_ACTIVE){
    echo "<a href='logout.php' class='logout'> Kijelentkezés </a>";
    echo  '&nbsp&nbsp  Üdv ' ;
    echo $_SESSION['name'];
     }
    }else{
      echo "<a href='login.php' class='logout'> Bejelentkezés</a>" ;  
    }
  
       
      
      ?>
      
     
    </div>
  </nav>


<div class="contentForm" >
 
      
<div class="col-1 mx-auto" >

    <div class="jumbotron">
  
      <form class="loginform" action = "validation.php" method="POST">
      <div class="form-group" style="padding-top: 80px;">
      <h2>Bejelentkezés</h2>
      <?php if (isset($_GET['error'])) { ?>
     		<p class="error"><?php echo $_GET['error']; ?></p>
     	<?php } ?>
         
        <label for="email">Email cím</label>
        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Email cím" 
        value="<?php echo isset($_POST["email"]) ? $_POST["email"] : '';  ?>" required >
      </div>
      <div class="form-group">
        <label for="name">Felhasználónév</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="név"
        value="<?php echo isset($_POST["name"]) ? $_POST["name"] : '';  ?>" required>
      </div>
      <div class="form-group">
        <label for="password">Jelszó</label>
        <input type="password" class="form-control" id="pass" name="pass" placeholder="jelszó"
        value="<?php echo isset($_POST["pass"]) ? $_POST["pass"] : '';  ?>"required>
      </div>
      <button type="submit" name="submit" value="1" class="btn btn-primary">Bejelentkezés</button>
    </form>
    </div>
</div>      
<p class="hint " style="padding-left: 800px; padding-top: 50px;">Nincs fiókja?<a href="contact.php">Regisztráljon</a></p>

</div>    


  
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<footer class="footer col-md-8 col-lg-4 ml-auto" style="padding-left: 50px;">
  <p> 2021 - Company &copy All rights reserved</p>
  
  </footer>
</body>
</html>









