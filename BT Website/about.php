<?php
    session_start();
   // if(!isset($_SESSION['name'])){
//header("location:login.php");
    //}
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BT-About</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
     integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
     <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
 <!--navbar-->
 <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="images/logo.jpg"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-200px" >
          <li class="nav-item">
            <a class="nav-link " href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="about.html">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Products
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="#">Crime City-Mobile</a></li>
              <li><a class="dropdown-item" href="#">Fighters War</a></li>
              <li><a class="dropdown-item" href="soma.php">Soma</a></li>
            </ul>
            
          </li>
          <li class="nav-item">
          <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='message.php'>Contact</a>";
           }
        }else{
             
            echo "<a class='nav-link active' href='contact.php'>Contact</a>";
           }
        
            ?>
          </li>

          <li > <?php
          if(isset($_SESSION['id'])){
           if(session_status() == PHP_SESSION_ACTIVE){
            echo "<a class='nav-link active' href='user.php'>Profile</a>";
           }
        }else{
             
            echo " ";
           }
        
            ?> </li>
        </ul>  

      
      </div>
         
      <?php
     if(isset($_SESSION['id'])){
     if(session_status() == PHP_SESSION_ACTIVE){
    echo "<a href='logout.php' class='logout'> Kijelentkezés </a>";
    echo  '&nbsp&nbsp  Üdv ' ;
    echo $_SESSION['name'];
     }
    }else{
      echo "<a href='login.php' class='logout'> Bejelentkezés</a>" ;  
    }
  
       
      
      ?>
    </div>
  </nav>



    <div class="content"  style=height:100%>

      
        
    <!--Slidee-->
 <div class="row">


<div class="col-lg-8">



            <div id="carouselExampleControls" class="carousel slide col-lg-8 col-md-12" data-bs-ride="carousel" style="padding-left: 50px;" >
    <div class="carousel-inner" >
      <div class="carousel-item active">
        <img src="images/office1.jpg" height="600"  class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/office2.jpg" height="600"  class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/office3.jpg " height="600" class="d-block w-100" alt="...">
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="prev" style="padding-left: 50px;">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"  data-bs-slide="next" style="padding-left: 50px;">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>

</div>

  
<div class="col-lg">
  <h1 class="company col-lg " style="padding: auto;">Company</h1>

    <p class="about col-lg-12 col-sm-12 " style="padding-right: 100px;">
        An office is generally a building, room or other area where an organization's employees perform administrative work in order to support and realize
         objects and goals of the organization. The word "office" may also denote a position within an organization with specific duties attached to it 
         (see officer, office-holder, official); the latter is in fact an earlier usage, office as place originally referring 
         to the location of one's duty. When used as an adjective, the term "office" may refer to business-related tasks.
          In law, a company or organization has offices in any place where it has an official presence, even if 
          that presence consists of (for example) a storage silo rather than an establishment with desk-and-chair. 
          An office is also an architectural and design phenomenon: ranging from a small office such as a bench in 
          the corner of a small business of extremely small size (see small office/home office), through entire floors of buildings,
           up to and including massive buildings dedicated entirely to one company. In modern terms an office is usually the location where 
           white-collar workers carry out their functions. According to James Stephenson, "Office is that part of business enterprise which
            is devoted to the direction and co-ordination of its various activities."
    </p>

  </div>

  </div>



   <!--gamedev-->
<div class="row">


  <div class="col-lg-8">
  
<img src="images/gamedev.png" class="col-lg-8 col-md-12 col-xs-12" width="720px" height="500px" style="padding-left:50px">

</div>


<div class="col-lg">
  <h1 class="company col-lg " style="padding: auto;">Game Development</h1>
<p class="about" style="padding-right: 100px;">Video game development is the process of developing a video game.
  Video game development is the process of developing a video game.
       The effort is undertaken by a developer, ranging from a single person to an international team dispersed across the globe.
        Development of traditional commercial PC and console games is normally funded by a publisher, and can take several years to reach completion. 
        Indie games usually take less time and money and can be produced by individuals and smaller developers. 
        The independent game industry has been on the rise,
         facilitated by the growth of accessible game development software such as Unity platform and Unreal Engine
          and new online distribution systems such as Steam and Uplay, as well as the mobile game market for Android and iOS devices.
  </p>
</div>
</div>



   <!--webdev-->


<div class="row">

  <div class="col-lg-8">

  <img src="images/webdev.jpg" class="col-lg-8 col-md-12 col-sm-12" width="720px" height="500px" style="padding-left:50px">
  
</div>

<div class="col-lg">
    <h1 class="company" style="padding: auto;">Web Development</h1>
  <p class="about" style="padding-right: 100px;">
    Web development is the work involved in developing a Web site for the Internet (World Wide Web) or an intranet (a private network).
     Web development can range from developing a simple single static page of plain text to complex Web-based Internet applications (Web apps), 
     electronic businesses, and social network services. A more comprehensive list of tasks to which Web development commonly refers, 
     may include Web engineering, Web design, Web content development, client liaison, client-side/server-side scripting, Web server and network 
     security configuration, and e-commerce development..</p>
  
    </div>


    </div>



    <!--mobdev-->
      
<div class="row">




<div class="col-lg-8">

    <img src="images/mobdev.jpg" class="col-lg-8 col-md-12 col-sm-12" width="720px" height="500px" style="padding-left:50px">
    
  </div>
    
  <div class="col-lg">
      <h1 class="company" style="padding: auto;">Mobile Development</h1>
    <p class="about" style="padding-right: 100px;">
      Mobile app development is the act or process by which a mobile app is developed for mobile devices, such as personal digital assistants,
   enterprise digital assistants or mobile phones. These applications can be pre-installed on phones during manufacturing platforms,
    or delivered as web applications using server-side or client-side processing (e.g., JavaScript) to provide an "application-like" 
    experience within a Web browser. Application software developers also must consider a long array of screen sizes, hardware 
    specifications, and configurations because of intense competition in mobile software and changes within each of the platforms.
     Mobile app development has been steadily growing, in revenues and jobs created. A 2013 analyst report estimates there are 529,000 
     direct app economy jobs within the EU then 28 members (including the UK), 60 percent of which are mobile app developers
      </p>
    
    </div>


    </div>




    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
     integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
     <script>

     </script>


<footer class="footer col-md-8 col-lg-4 ml-auto" style="padding-left: 50px;">
    <p> 2021 - Company &copy All rights reserved</p>
    
    </footer>
    
</body>
</html>